package org.example;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.Assert.*;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    UserService userService;

    @MockBean
    UserDb userDb = Mockito.mock(UserDb.class);

    @Test
    public void shouldReturnTrueIfPasswordIs8rMoreCharacters() throws Exception {

        when(userDb.changePassword("Markos","123456789")).thenReturn(true);
        Boolean expected = userService.changePassword("Markos","123456789");
        assertEquals(true,expected);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionForWeakPassword() throws Exception{
        userService.changePassword("Markos","123456");
        fail("Exception expected");
    }
}