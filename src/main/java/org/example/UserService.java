package org.example;

public class UserService {

    private UserDb userDb;

    public UserService(UserDb userDb) {
        this.userDb = userDb;
    }

    public boolean changePassword(String username, String password) throws Exception {
        if(password.length() > 8){
            return userDb.changePassword(username,password);
        }
        throw new Exception("weak password");
    }
}
